<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
	Manage multiple hostnames (domains, sub-domains) within a single instance of CodeIgniter.

	Url: https://gist.github.com/chill117/5971561

	Example:

		If you had the following domain/sub-domain style for your site:

		your-domain.com
		api.your-domain.com
		shop.your-domain.com


		Create the following sub-directories (+ files) in your application/controllers directory:

		application/controllers/home.php
		application/controllers/api/product.php
		application/controllers/api/products.php
		application/controllers/shop/catalog.php


		And, in your application/config/hosts.php file:

		$config['home'] = 'your-domain.com';
		$config['api'] = 'api.your-domain.com';
		$config['shop'] = 'shop.your-domain.com';


		Now if you navigate to your site in a browser, here's what you should get:

		your-domain.com -> Your site's home page
		api.your-domain.com/product -> The product end-point of your API
		api.your-domain.com/products -> The products end-point of your API
		shop.your-domain.com/catalog -> The catalog page of your shop

*/

class Domains {

	protected $domains;
	protected $config;

	/**
	 * Подготовка доменов
	 */
	protected function prepareDomains() {
		$this->config =& load_class('Config', 'core');

		$this->config->load('domains');

		$this->domains = $this->config->item('domains');
	}

	/**
	 * Проверка домена
	 */
	public function checkDomain() {
		$this->prepareDomains();

		$domain = (isset($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : '';

		if (!empty($this->domains[$domain])) {
			$this->config->set_item('base_url', $this->domains[$domain]);
		}

	}

}


/* End of file HostNameRouter.php */
/* Location: ./application/hooks/HostNameRouter.php */
