<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Доработка функционала языкового класса
 */
class MY_Lang extends CI_Lang {
	// --------------------------------------------------------------------

	/**
	 * Load a language file
	 *
	 * @access	public
	 * @param	mixed	the name of the language file to be loaded. Can be an array
	 * @param	string	the language (english, etc.)
	 * @param	bool	return loaded array of translations
	 * @param 	bool	add suffix to $langfile
	 * @param 	string	alternative path to look for language file
	 * @param 	string 	Альтернативный язык для загрузки
	 * @return	mixed
	 */
	function load($langfile = '', $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '', $alt_idiom = '') {
		if (is_array($langfile)) {
			foreach ($langfile as $value) {
				$this->load($value, $idiom, $return, $add_suffix, $alt_path);
			}

			return;
		}

		$params = array(
			'langfile' => $langfile,
			'idiom' => $idiom,
			'return' => $return,
			'add_suffix' => $add_suffix,
			'alt_path' => $alt_path,
			'alt_idiom' => $alt_idiom,
		);

		$langfile = str_replace('.php', '', $langfile);

		if ($add_suffix == TRUE) {
			$langfile = str_replace('_lang.', '', $langfile) . '_lang';
		}

		$langfile .= '.php';

		if (in_array($langfile, $this->is_loaded, TRUE)) {
			return;
		}

		$config = & get_config();

		if ($idiom == '') {
			$deft_lang = (!isset($config['language'])) ? 'english' : $config['language'];
			$idiom = ($deft_lang == '') ? 'english' : $deft_lang;
		}

		// Determine where the language file is and load it
		if ($alt_path != '' && file_exists($alt_path . 'language/' . $idiom . '/' . $langfile)) {
			include($alt_path . 'language/' . $idiom . '/' . $langfile);
		} else {
			$found = FALSE;

			foreach (get_instance()->load->get_package_paths(TRUE) as $package_path) {
				if (file_exists($package_path . 'language/' . $idiom . '/' . $langfile)) {
					include($package_path . 'language/' . $idiom . '/' . $langfile);
					$found = TRUE;
					break;
				}
			}

			if ($found !== TRUE) {
				if ($alt_idiom != '') {
					$this->load($params['langfile'], $params['alt_idiom'], $params['return'], $params['add_suffix'], $params['alt_path']);
					log_message('error', 'Unable to load the requested language file: language/' . $idiom . '/' . $langfile);
					return;
				}
				log_message('error', 'Unable to load the requested language file: language/' . $idiom . '/' . $langfile);
			}
		}


		if (!isset($lang)) {
			log_message('error', 'Language file contains no data: language/' . $idiom . '/' . $langfile);
			return;
		}

		if ($return == TRUE) {
			return $lang;
		}

		$this->is_loaded[] = $langfile;
		$this->language = array_merge($this->language, $lang);
		unset($lang);

		log_message('debug', 'Language file loaded: language/' . $idiom . '/' . $langfile);
		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Fetch a single line of text from the language array
	 *
	 * @access	public
	 * @param	string	$line	the language line
	 * @return	string
	 */
	function line($line = '', $log_errors = TRUE) {
		$value = ($line == '' OR ! isset($this->language[$line])) ? FALSE : $this->language[$line];

		// Because killer robots like unicorns!
		if ($value === FALSE && $log_errors === TRUE) {
			log_message('error', 'Could not find the language line "' . $line . '"');
		}

		if ($value === false) {
			$value = (string) $line;
		}

		return $value;
	}

	/**
	 * Проверка наличия ключа в данных
	 *
	 * @access	public
	 * @param	string	$line	the language line
	 * @return	bool
	 */
	function checkLine($line = '') {
		return isset($this->language[$line]) ? TRUE : FALSE;
	}

	/**
	 * Получение всех данных
	 * @return array
	 */
	public function getAllLine() {
		return $this->language;
	}

}
