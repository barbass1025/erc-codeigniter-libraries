<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Доработка функционала Input класса
 */
class MY_Input extends CI_Input {
	/**
	 * Constructor
	 *
	 * @access	public
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Публичный метод для доступа к методу _clean_input_data для очистки данных
	 * @param mixed (string|array)
	 * @return mixed (string|array)
	 */
	public function filter_data($data) {
		return $this->_clean_input_data($data);
	}
}
