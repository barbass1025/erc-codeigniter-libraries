<?php

/**
 * Создание границ времени на основе параметров
 * @param string $date_from
 * @param string $date_to
 * @return array
 */
function border_date($date_from = null, $date_to = null) {
	if (!$date_from) {
		$date_from = date('Y-m-d H:i:s');
	}
	$time_from = strtotime($date_from);

	if (!$date_to) {
		$date_to = date('Y-m-d H:i:s');
	}
	$time_to = strtotime($date_to);

	if (date('Y-m-d', $time_from) === date('Y-m-d', $time_to)) {
		if (date('H:i:s', $time_from) !== date('H:i:s', $time_to)) {
			if (date('H:i:s', $time_to) === '00:00:00') {
				$date_to = date('Y-m-d', $time_to) . ' ' . '23:59:59';
			}
		} else {
			$date_to = date('Y-m-d', $time_to) . ' ' . '23:59:59';
		}
	}

	return array(
		'date_from' => $date_from,
		'date_to' => $date_to,
	);
}

/**
 * Возвращает текущую дату в стандартном mysql/linux формате
 * @param string $date Дата
 * @param string $format Формат
 * @return string
 */
function default_date($date = null, $format = 'Y-m-d H:i:s') {
	$time = ($date) ? strtotime($date) : time();
	return date($format, $time);
}

/**
 * Список дат между периодами
 * @param string $date_from Период от
 * @param string $date_to Период до
 * @return array
 */
function date_list_between_date($date_from, $date_to = null) {
	if (empty($date_from) || date('Y-m-d', strtotime($date_from)) === '1970-01-01') {
		$date_from = date('Y-m-d');
	}
	if (empty($date_to) || date('Y-m-d', strtotime($date_to)) === '1970-01-01') {
		$date_to = date('Y-m-d');
	}

	$from = new \DateTime(date('Y-m-d', strtotime($date_from)));

	$date_to = strtotime('+1 day', strtotime($date_to));
	$to = new \DateTime(date('Y-m-d', $date_to));

	$period = new \DatePeriod($from, new DateInterval('P1D'), $to);

	$arrayOfDates = array_map(
		function($item) {
		return $item->format('Y-m-d');
	}, iterator_to_array($period)
	);

	return $arrayOfDates;
}
