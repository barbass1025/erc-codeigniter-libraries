<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Класс построения элементов таблиц по стандартной структуре
 */
class HTable {

	/**
	 * Создание заголовков таблиц
	 * @param array $td_list Список ячеек
	 * @param array $form Данные формы
	 * $td_list = array(
	 * 	array(
	 * 		'text' => 'Name',
	 * 		'url' => 'http://site.com/',
	 * 		'order' => 'name',
	 * 	),
	 * 	...
	 * )
	 */
	static function createThead($td_list = array(), $form = array()) {
		$thead = "<thead><tr>";

		$thead .= self::createTdListForThead($td_list, $form);

		$thead .= "</tr></thead>";

		return $thead;
	}

	/**
	 * Создание ячеек заголовков таблицы
	 * @param array $td Список ячеек
	 * @param array $form Данные формы
	 * @return string
	 */
	static function createTdListForThead($td_list, $form) {
		$thead = "";

		foreach ($td_list as $td) {
			$thead .= self::createTdForThead($td, $form);
		}

		return $thead;
	}

	/**
	 * Создание ячейки заголовка таблицы
	 * @param array $td Список ячеек
	 * @param array $form Данные формы
	 * @return string
	 * $td = array(
	 * 	'text' => 'Name', required
	 * 	'url' => 'http://site.com/',
	 * 	'order' => 'name',
	 * )
	 */
	static function createTdForThead($td, $form) {
		$th_tmp_order = "<th>
			<a href='%s'>
				<span class='text'>%s</span>
				%s
			</a>
		</th>";

		$th_tmp = "<th><span class='text'>%s</span></th>";

		if (empty($td['text'])) {
			return "<th></th>";
		}

		$arrow = '';
		if (isset($form['order']) && isset($td['order']) && $form['order'] === $td['order']) {
			if (isset($form['by']) && $form['by'] === 'asc') {
				$arrow = "<span class='glyphicon glyphicon-arrow-down'></span>";
			} elseif (isset($form['by']) && $form['by'] === 'desc') {
				$arrow = "<span class='glyphicon glyphicon-arrow-up'></span>";
			}
		}

		if (isset($td['url'])) {
			$th = sprintf($th_tmp_order, $td['url'], $td['text'], $arrow);
		} else {
			$th = sprintf($th_tmp, $td['text']);
		}

		return $th;
	}

	/**
	 * Создание строки с формами для поиска
	 * @param array $td_list Список ячеек
	 * @param array $form Данные формы
	 * $td_list = array(
	 * 	array(
	 * 		'type' => 'text',
	 * 		'name' => 'Form[name]',
	 * 		'value' => '',
	 * 	),
	 * 	...
	 * )
	 */
	static function createTheadSearch($td_list, $form) {
		$tr = "<tr>";

		$tr .= self::createTdListForSearch($td_list, $form);

		$tr .= "</tr>";

		return $tr;
	}

	/**
	 * Создание ячеек с формой для поиска
	 * @param array $td_list Данные для ячейки
	 * @param array $form
	 * @return string
	 */
	static function createTdListForSearch($td_list, $form) {
		$tr = "";

		foreach ($td_list as $td) {
			$tr .= self::createTdForSearch($td, $form);
		}

		return $tr;
	}

	/**
	 * Создание ячейки с формой для поиска
	 * @param array $td Данные для ячейки
	 * @param array $form
	 * @return string
	 * $td = array(
	 * 	'type' => 'text',
	 * 	'name' => 'Form[name]',
	 * 	'value_index' => name
	 * 	'value' => '',
	 * )
	 */
	static function createTdForSearch($td, $form = array()) {
		if (empty($td['name'])) {
			return "<th></th>";
		}
		if (empty($td['type'])) {
			$td['type'] = 'text';
		}

		if (!isset($td['value'])) {
			if (isset($td['value_index']) && isset($form[$td['value_index']])) {
				$val = $form[$td['value_index']];
			} elseif (isset($form[$td['name']])) {
				$val = $form[$td['name']];
			}
			$td['value'] = isset($val) ? $val : '';
		}
		if ($td['type'] !== 'select') {
			$td_tmp = '<th>
				<input class="form-control input-sm"
				type="%s"
				maxlength="50"
				name="%s"
				value="%s">
			</th>';
		} else {
			$td_tmp = '<th>'
				. '<select class="form-control input-sm" name="' . $td['name'] . '" ' . ((!empty($td['multiple'])) ? 'multiple' : '') . '>'
				. '<option value="" selected>' . _l('select_default') . '</option>';

			if (!empty($td['option_list'])) {
				foreach ($td['option_list'] as $key => $value) {
					$td_tmp .= '<option value="' . $value . '" ' . ((isset($td['value']) && $td['value'] === $value) ? 'selected' : '') . '>' . $key . '</option>';
				}
			}

			$td_tmp .= '</select>'
				. '</th>';
		}

		return sprintf($td_tmp, $td['type'], $td['name'], $td['value']);
	}

	/**
	 * Создание блока действий
	 * @param array $data
	 * @return string
	 * $data = array(
	 * 	'filter_title' => string,
	 * 	'reset_title' => string,
	 * 	'reset_url' => string,
	 * 	'td_type' => string (td, th, '')
	 * )
	 */
	static function createTdAction($data = array(), $form = array()) {
		$data['filter_title'] = (isset($data['filter_title'])) ? $data['filter_title'] : _l('button_filter');
		$data['reset_title'] = (isset($data['reset_title'])) ? $data['reset_title'] : _l('button_reset');
		$data['reset_url'] = (isset($data['reset_url'])) ? $data['reset_url'] : '';
		$data['td_type'] = (isset($data['td_type']) && in_array($data['td_type'], array('td', 'th', ''))) ? $data['td_type'] : 'th';
		$data['limit'] = (!empty($data['limit'])) ? true : false;

		$th_tmp = (!empty($data['td_type'])) ? '<' . $data['td_type'] . '>' : '';

		if ($data['limit']) {
			$th_tmp .= '<select name="Form[limit]" class="form-control input-sm">';
			$arr_limit = array(
				'', 10, 20, 50, 100, 200, 1000
			);

			foreach ($arr_limit as $l) {
				$selected = (isset($form['limit']) && $form['limit'] == $l) ? 'selected' : '';

				$th_tmp .= '<option value="' . $l . '" ' . $selected . '>' . $l . '</option>';
			}

			$th_tmp .= '</select>';
		}

		$th_tmp .= '
			<button type="submit" class="btn btn-link" title="%s">
				<span class="glyphicon glyphicon-filter"></span>
			</button>

			<a href="%s" title="%s">
				<span class="glyphicon glyphicon-remove"></span>
			</a>';

		$th_tmp .= (!empty($data['td_type'])) ? '</' . $data['td_type'] . '>' : '';

		return sprintf($th_tmp, $data['filter_title'], $data['reset_url'], $data['reset_title']);
	}

	/**
	 * Создание ссылок
	 * @param array
	 * @return string
	 */
	static function createA($data = array()) {
		$a = '<a ';

		if (!empty($data['url'])) {
			$a .= ' target="_blank" href="' . $data['url'] . '" ';
		} else {
			$a .= ' href="javascript:void(0);" ';
		}
		if (!empty($data['title'])) {
			$a .= ' title="' . $data['title'] . '" ';
		}
		if (!empty($data['class'])) {
			$a .= ' class="' . $data['class'] . '" ';
		}
		if (!empty($data['id'])) {
			$a .= ' data-id="' . $data['id'] . '" ';
		}
		if (!empty($data['data'])) {
			$a .= $data['data'];
		}

		$a .= '>';

		if (!empty($data['icon'])) {
			$a .= '<span class="glyphicon glyphicon-' . $data['icon'] . '" aria-hidden="true"></span>';
		}

		if (!empty($data['content'])) {
			$a .= $data['content'];
		}

		$a .= '</a>';

		return $a;
	}

	/**
	 * Создание массива ссылок
	 * @param array
	 * @return string
	 */
	static function createAList($a_list) {
		$a_l = array();
		foreach ($a_list as $a) {
			$a_l[] = self::createA($a);
		}

		$a_l = implode(' / ', $a_l);

		return $a_l;
	}

}
