<?php

/**
 * Заменяет порядковый номер массива на значение индекса и соответсвущющее значение
 * Example: $a = array(array(
 * 	'id' => 56,
 * 'name' => 'Test'
 * ))
 * 	Result: $result = array(56 => array(
 * 	'id' => 56,
 * 	'name' => 'Test'
 * ))
 * @param array $data
 * @param string $id
 * @return array
 */
function array_change_index_on_id(array $data, $id) {
	$result = array();

	foreach ($data as $i => $d) {
		$key = (isset($d[$id])) ? $d[$id] : $i;

		$result[$key] = $d;
	}

	return $result;
}

/**
 * Удаление лишних параметров
 *
 * @param array $data массив Данных
 * @param mixed (array | null) $retain Массив индексов для сохранения элементов
 * @param mixed (array | null) $remove Массив индексов для удаления элементов
 */
function array_delete_index(array $data, $retain = null, $remove = null) {
	$result = array();

	foreach ($data as $key => $value) {
		if (is_array($remove) && in_array($key, $remove)) {
			continue;
		}

		if (is_array($retain) && in_array($key, $retain)) {
			$result[$key] = $value;
		} elseif ($retain === null) {
			$result[$key] = $value;
		}
	}

	return $result;
}

/**
 * Получение значений по полю
 * @param array $data
 * @param string $field
 * @param bool $distinct Игнорировать дубликаты
 * @return array
 */
function array_data_by_field(array $data, $field = '', $distinct = false) {
	$data_field = array();

	foreach ($data as $d) {
		if (array_key_exists($field, $d)) {
			if ($distinct) {
				$data_field[$d[$field]] = $d[$field];
			} else {
				$data_field[] = $d[$field];
			}
		}
	}

	return $data_field;
}

/**
 * Формирование данных для autocomplete
 * @param array $data
 * @param mixed (string|array) $fields Поля
 * @param string $label Поле для label
 */
function array_data_to_autocomplete(array $data, $fields, $label) {
	$autocomplete = array();

	$fields = (array) $fields;

	$i = 0;
	foreach ($data as $d) {
		foreach ($d as $key => $val) {
			if (in_array($key, $fields)) {
				$autocomplete[$i][$key] = $val;
			}
			if ($key == $label) {
				$autocomplete[$i]['label'] = $val;
			}
		}

		$i++;
	}

	return $autocomplete;
}

/**
 * Приведение элементов массива к простым типам
 * @param array $data array
 * @param string $type
 * @return array
 * Example: вложенные массивы приводит к указанному типу
 */
function array_data_to_type(array $data, $type = 'int') {
	$type = (in_array($type, array('int', 'float', 'string'))) ? $type : 'int';

	if (!is_array($data)) {
		$data = (array) $data;
	}

	foreach ($data as &$d) {
		if (is_array($d)) {
			$d = array_data_to_type($d, $type);
			continue;
		}

		switch ($type) {
			case 'int':
				$d = (int) $d;
				break;
			case 'float':
				$d = (float) $d;
				break;
			case 'string':
				$d = (string) $d;
				break;
			default:
				$d = (int) $d;
				break;
		}
	}

	return $data;
}

/**
 * Поиск по массиву аналогично поиску в базе данных
 * @param array $search_arr Массив с условиями
 * @param array $data_arr Данные
 * @param $strict_mode Строгий поиск
 * @param $save_index В ответе сохранить индексы
 * @return array
 */
function search_in_array(array $search_arr, array $data_arr, $strict_mode = false, $save_index = false) {
	$result_index = array();

	foreach ($data_arr as $i => $data) {
		$result_index[$i] = 0;

		foreach ($search_arr as $key => $val) {
			if (!isset($data[$key])) {
				continue;
			}

			if ($strict_mode && $data[$key] === $val) {
				$result_index[$i] += 1;
			} elseif (!$strict_mode && $data[$key] == $val) {
				$result_index[$i] += 1;
			}
		}
	}

	$result = array();

	foreach ($result_index as $i => $count) {
		if ($count === count($search_arr)) {
			if ($save_index) {
				$result[$i] = $data_arr[$i];
			} else {
				$result[] = $data_arr[$i];
			}
		}
	}

	return $result;
}

/**
 * Удаление из массива значений
 * @param type $data Массив
 * @param type $value Значение
 * @param type $strict_mode Строгость проверки
 * @return array
 */
function array_clear($data, $value, $strict_mode = false) {
	$result = [];

	foreach ($data as $val) {
		if ($strict_mode && $val === $value) {
			continue;
		} elseif (!$strict_mode && $val == $value) {
			continue;
		} else {
			$result[] = $val;
		}
	}

	return $result;
}
