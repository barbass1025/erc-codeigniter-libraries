<?php

/**
 * Синоним для $this->lang->line()
 * @param string $key Ключ
 * @param $args Параметры для форматирования строки
 * @return string
 */
function _l($key = '', $args = array()) {
	$CI = &get_instance();

	$string = $CI->lang->line((string) $key);

	if ($args) {
		$string = vsprintf($string, $args);
	}

	return $string;
}

/**
 * Приведение фамилии/имени/отчества к формату ПУШКИН А.С.
 * @param string $surname
 * @param string $firstname
 * @param string $middlename
 * @param bool $register_upper Перевод в верхний регистр
 * @return string
 */
function str_to_fio($surname = '', $firstname = '', $middlename = '', $register_upper = false) {
	$surname = trim($surname);
	$firstname = trim($firstname);
	$middlename = trim($middlename);

	$str = $surname . ' ' . mb_substr($firstname, 0, 1) . '.';
	if (!empty($middlename)) {
		$str .= ' ' . mb_substr($middlename, 0, 1) . '.';
	}

	if ($register_upper) {
		$str = mb_strtoupper($str);
	}

	return $str;
}

/**
 * Функция очищает строку от всех спец символов (кроме _ и -)
 * @param string Строка
 * @param string Маска поиска
 * @param string Строка замены
 * @return string
 */
function clearString($str, $mask = '/[^\w-]/iu', $replace = ' ') {
	return preg_replace($mask, $replace, $str);
}

/**
 * Удаление ведущих нулей
 * @param string
 * @return string
 * 0001 -> 1
 */
function deleteLeadNull($str) {
	return preg_replace("/^0*/i", "", $str);
}
