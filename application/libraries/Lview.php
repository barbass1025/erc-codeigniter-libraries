<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Работа с отображениями
 */
class LView {

	protected $CI = null;
	protected $_styles = array();
	protected $_js = array();
	protected $_title = '';
	protected $_views = array();
	protected $_theme = 'default';
	protected $_layout = 'common/layout/main';

	public function __construct() {
		$this->CI = & get_instance();
	}

	/**
	 * Отрендерить только указанный файл
	 * @param string Шаблон
	 * @param mixed Данные для шаблона
	 * @param bool Вывести данные в клиент
	 */
	public function renderPath($view, $data, $render = true) {
		if (!empty($this->_theme)) {
			$file = $this->_theme . '/' . $view;
		} else {
			$file = $view;
		}

		if (isset($data['text']) && is_array($data['text'])) {
			$data['text'] = array_merge($this->setAppendText(), $data['text']);
		} else {
			$data['text'] = $this->setAppendText();
		}

		if ($render) {
			$this->CI->load->view($file, $data, false);
		} else {
			return $this->CI->load->view($file, $data, true);
		}
	}

	/**
	 * Отрендерить весь шаблон
	 * @param string Шаблон
	 * @param mixed Данные для шаблона
	 * @param bool Вывести данные в клиент
	 */
	public function render($view = null, $data = null, $render = true) {
		$sort_views = array();

		if (!empty($view)) {
			$this->addView($view, $data);
		}

		$content = '';

		foreach ($this->_views as $v) {
			$content .= $this->renderPath($v['view'], $v['data'], false);
		}

		$data_full['content'] = $content;
		$data_full['js'] = $this->_js;
		$data_full['styles'] = $this->_styles;
		$data_full['title'] = $this->_title;

		if (!empty($this->_theme)) {
			$layout = $this->_theme . '/' . $this->_layout;
		} else {
			$layout = $this->_layout;
		}

		if ($render) {
			$this->CI->load->view($layout, $data_full, false);
		} else {
			return $this->CI->load->view($layout, $data_full, true);
		}
	}

	/**
	 * Добавление шаблона
	 * @param string | array $view
	 * @param array $data
	 * @param string $type
	 * @return self
	 */
	public function addView($view, $data = array()) {
		if (is_array($view)) {
			foreach ($view as $v) {
				if (!isset($v[0])) {
					break;
				}
				$this->_views[] = array(
					'view' => $strval($v[0]),
					'data' => (isset($v[1]) && is_array($v[1])) ? $v[1] : array(),
				);
			}
		} else {
			$this->_views[] = array(
				'view' => strval($view),
				'data' => $data,
			);
		}
		return $this;
	}

	/**
	 * Добавление стиля  к выгрузке
	 * @param string $style Название стиля
	 * @param bool $ignore Не использовать дефолтный путь к стилям
	 * @return self
	 */
	public function addStyle($style, $ignore = false) {
		$this->_styles[strval($style)] = (!$ignore) ? base_url('resource/css/' . $style) : base_url($style);
		return $this;
	}

	/**
	 * Добавление стиля  к выгрузке
	 * @param string $style Название стиля
	 * @param bool $ignore Не использовать дефолтный путь к стилям
	 * @return self
	 */
	public function addJs($js, $ignore = false) {
		$this->_js[strval($js)] = (!$ignore) ? base_url('resource/js/' . $js) : base_url($js);
		return $this;
	}

	/**
	 * Добавление плагина на страницу
	 * @param string $plugin_name Наименование плагина
	 */
	public function addPlugin($plugin_name) {
		$plugins = $this->CI->config->item('plugins', 'frontend');

		$plugin = (isset($plugins[$plugin_name])) ? $plugins[$plugin_name] : null;

		if (!$plugin) {
			return false;
		}

		if (isset($plugin['css'])) {
			foreach ($plugin['css'] as $css) {
				$this->addStyle($css, true);
			}
		}
		if (isset($plugin['js'])) {
			foreach ($plugin['js'] as $js) {
				$this->addJs($js, true);
			}
		}

		return $this;
	}

	/**
	 * Установка заголовка для страницы
	 * @param string $title
	 * @return self
	 */
	public function setTitle($title = '') {
		/*
		  Установка перед основным титлом название сервиса
		  $this->_title = $this->CI->config->item('');
		 */

		if ($title) {
			$this->_title .= strval($title);
		}

		return $this;
	}

	/**
	 * Установка темы сайта
	 * @param string $theme
	 * @return self
	 */
	public function setTheme($theme) {
		$this->_theme = strval($theme);

		return $this;
	}

	/**
	 * Сбрасывает все параметры
	 * @return self
	 */
	public function clear() {
		$this->_styles = array();
		$this->_js = array();
		$this->_title = '';
		$this->_views = array();
		//$this->_theme = 'default';

		return $this;
	}

	/**
	 * Подготовка данных для поиска/фильтра
	 * @param string $key_form - В каком ключе заключены данные
	 * @param array $keys_access - Массив разрешенных ключей
	 */
	public function getFormSearch($key_form = 'Form', $keys_access = array()) {
		$form = ($this->CI->input->get_post((string) $key_form)) ? $this->CI->input->get_post((string) $key_form, true) : array();
		$result = array(
			'url' => '?page={page}',
			'form' => array(),
			'page' => 0,
		);
		$implode = array();

		// удаляем лишние ключи или пустые значения
		foreach ($form as $key => $value) {
			if ($keys_access && !in_array($key, $keys_access)) {
				unset($form[$key]);
				continue;
			}

			if ($value === '' ||
				(
				is_array($value) &&
				empty($value)
				)
			) {
				unset($form[$key]);
			}
		}


		$result['form'] = $form;

		$implode = $this->processSearchData($form, $key_form);

		if ($result['form']) {
			$result['url'] = '?' . http_build_query($implode) . '&page={page}';
		} else {
			$result['url'] = '?page={page}';
		}

		if ($this->CI->input->get_post('page')) {
			$result['form']['page'] = (int) $this->CI->input->get_post('page');
			$result['page'] = $result['form']['page'];
		}

		return $result;
	}

	/**
	 * Превращение массива данных в url
	 * @param array $data Входящие данные
	 * @param string $key Ключ для подмассивов
	 * @return array
	 * array(key => value)
	 */
	private function processSearchData($data = array(), $key_array = '') {
		$array = array();

		foreach ($data as $key => $value) {
			if (!$key_array) {
				$fullkey = $key;
			} else {
				$fullkey = $key_array . '[' . $key . ']';
			}

			if (is_array($value)) {
				$array_tmp = $this->processSearchData($value, $fullkey);
				$array = array_merge($array, $array_tmp);
			} else {
				if (!$key_array) {
					$array[$fullkey] = $value;
				} else {
					$array[$fullkey] = $value;
				}
			}
		}

		return $array;
	}

	/**
	 * Генераци урла для сортировки
	 * @param string $key Ключ параметра
	 * @param string $url Урл, из которого нужно извлечь данные
	 * @param string $key_form Ключ для массива
	 * @return string key=>value
	 */
	public function generateOrderByUrl($key, $url, $key_form = 'Form') {
		$order = 'order';
		$by = 'by';

		if (isset($url[0]) && $url[0] == '?') {
			$url = substr_replace($url, '', 0, 1);
		}

		parse_str((string) $url, $query);

		if ($key_form) {
			$query[$key_form][$order] = $key;
			if (isset($query[$key_form][$by])) {
				if ($query[$key_form][$by] == 'asc') {
					$query[$key_form][$by] = 'desc';
				} else {
					$query[$key_form][$by] = 'asc';
				}
			} else {
				$query[$key_form][$by] = 'asc';
			}
		} else {
			$query[$order] = $key;

			if (isset($query[$by])) {
				if ($query[$by] == 'asc') {
					$query[$by] = 'desc';
				} else {
					$query[$by] = 'asc';
				}
			} else {
				$query[$by] = 'asc';
			}
		}

		unset($query['page']);

		return http_build_query($query);
	}

	/**
	 * Установка общего шаблона
	 * @param string
	 */
	public function setLayout($layout) {
		$this->_layout = $layout;
		return $this;
	}

	public function setAppendText() {
		return array(
			'heading' => $this->_title,
		);
	}

}
