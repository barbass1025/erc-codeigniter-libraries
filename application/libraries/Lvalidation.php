<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Дополнительный класс валидация данных
 */
class LValidation {

	protected $CI = null;
	protected $_rules = array();

	public function __construct() {
		$this->CI = & get_instance();
	}

	/**
	 * Проверка иин
	 * @param string
	 * @return bool
	 *
	 * http://zloi.kz/blog/show/12.html
	  http://lagran.kz/archives/4794
	  Расшифровка ИИН :
	  первые 6 разрядов - это дата рождения ггммдд, то есть 12 августа 1985 года в ИИНе будет 850812
	  7 разряд отвечает за век рождения и пол. Если цифра нечетная - пол мужской, четная - женский. 1,2 - девятнадцатый век, 3,4 - двадцатый, 5,6 - двадцать первый.
	  8-11 разряды - заполняет орган Юстиции.
	  12 разряд - контрольная цифра, которая расчитывается по определенному алгоритму
	  Проверка корректности ИИН :
	  Вспомогательная проверка. Строится на расшифровке.
	  вычленяем дату рождения из ИИН и проверяем её. Если дата неверна, то ИИН может быть не верен
	  если 7 разряд равен 0 или больше 6, то ИИН может быть не верен
	  Почему я пишу "может быть не верен", да потому что все решает человеческий фактор! Руки у операторов не всегда откуда надо растут, в итоге появляются ИИН с неправильно написанными датами, вместо 850812 например напишут 120885. Или опечатаются где-нибудь.
	  Основная проверка. По контрольной цифре, 12 разряду. Алгоритм формирования

	  Формируем сумму. Сумма произведения порядка разряда на его значение. То есть для 850812 сумма начнется с (1*8+2*5+3*0+4*8+5*1+5*2+...). Берем остаток от делния суммы на 11. если остаток оказывается равен 10, то идем на второй шаг. Если не равен 10, то сравниваем контрольный разряд с остатком. Если равны - ИИН корректный.
	  Сюда мы попадаем если в результате первого шага мы получили остаток 10. Здесь мы тоже будем суммировать произведения значений разрядов, только на этот раз не с порядковыми номерами, а с весами разрядов. Вес формируется следующим образом - остаток от деления на 11  суммы (порядковый номер разряда + 2). То есть для 850812, первый вес равен 3, второй 4 и так далее. Если вес оказывается равным 0, то вес приравнивают к 11. ну а дальше также. берем остаток от деления получившейся суммы на 11, если получаем 10, то данный ИИН не используется. Если не 10 сравниваем с контрольным разрядом.
	 */
	public function iin($iin = '') {
		$iin = (string) $iin;
		if (strlen($iin) !== 12) {
			return false;
		}

		$s = 0;
		for ($i = 0; $i < 11; $i++) {
			$s = $s + ($i + 1) * $iin[$i];
		}
		$k = $s % 11;
		if ($k == 10) {
			$s = 0;
			for ($i = 0; $i < 11; $i++) {
				$t = ($i + 3) % 11;
				if ($t == 0) {
					$t = 11;
				}
				$s = $s + $t * $iin{$i};
			}
			$k = $s % 11;
			if ($k == 10) {
				return false;
			}

			return ($k == substr($iin, 11, 1));
		}
		return ($k == substr($iin, 11, 1));
	}

	/**
	 * Проверка мобильного телефона
	 * Формат: +7 1234567890
	 * @param string $mobile
	 * @return bool
	 */
	public function mobile($mobile = '') {
		return (preg_match('/^\+7\d{10}$/', (string) $mobile) == 1);
	}

	/**
	 * Проверка пароля на сложность: должен содержать буквы, цифры, символы разных регистров
	 * @param string $password
	 * @param string $password_repeat
	 * @return bool
	 */
	public function password($password = '', $password_repeat = null) {
		$password = (string) $password;

		// Если состоит только из буквы или цифр
		if (ctype_digit($password) ||
			preg_match("/^([а-яa-z])+$/i", $password)
		) {
			return false;
		}

		// Смотрим по регистру
		$password_lower = mb_strtolower($password, $this->CI->config->item('charset'));
		$password_upper = mb_strtoupper($password, $this->CI->config->item('charset'));
		if ($password_lower === $password ||
			$password_upper === $password
		) {
			return false;
		}

		if ($password_repeat !== null) {
			return ($password_repeat === $password);
		}

		return true;
	}

	/**
	 * Валидация даты
	 * @param string $val
	 * @retur bool
	 */
	public function date($val = '', $date_null = null) {
		$date = date('Y-m-d', strtotime($val));

		if ($date_null === true &&
			(
			$val === '0000-00-00' ||
			$val === '0000-00-00 00:00:00'
			)
		) {
			return true;
		}

		if ($date === '1970-01-01') {
			return false;
		}
		return true;
	}

	/**
	 * Валидация времени
	 * @param string $val
	 * @retur bool
	 */
	public function time($val = '', $time_null = null) {
		$time = date('H:i:s', strtotime($val));

		if ($time_null === true && (
			$val === '00:00:00' ||
			$val === '00:00' ||
			$val === '00' ||
			$val === '0'
			)
		) {
			return true;
		}

		if (
			(
			$val === '06:00:00' ||
			$val === '06:00' ||
			$val === '06' ||
			$val === '6'
			) && $time === '06:00') {
			return false;
		}
		return true;
	}

	/**
	 * Валидация даты
	 * @param string $data
	 * @retur bool
	 */
	public function json($data = '') {
		$json = @json_decode($data, true);
		if ($json === null || json_last_error() !== JSON_ERROR_NONE) {
			return false;
		}
		return true;
	}

	/**
	 * Установка правил для валидации
	 * @param array
	 * @return bool
	 * $data = array(
	 * 	array(
	 * 		'field' => 'name',
	 * 		'type' => 'number', default string (string, number, int, numeric, float, double, array, json, date, time)
	 * 		'min_length' => 1, default 1
	 * 		'max_length' => 150, default 150
	 * 		'error' => 'error_name', default 'error_data'
	 * 		'required' => null
	 * 		'exact_length' => 50 Для строк длина, для чисел значение
	 * 		'date' => bool, // Проверка на дату
	 * 		'date_null' => bool, // Дата может быть 0000-00-00
	 * 		'exist_in_collection' => array( // Массив для поиска в коллекции
	 * 			'class' => '', // Путь к классу
	 * 			'method' => null, // Альтернативный метод (по-умолчанию getTotal)
	 * 			'params' => array(), // Параметры для поиска, сюда по field вставится значение
	 * 			'field' => 'new_field', // Если поле имеет другое наименование
	 * 			'field_list_form' => [], // Поля формы, которые будут использоваться для поиска
	 * 			'cache' => false // Сохранять результат поиска во временный кеш (используется метод get)
	 * 		)
	 * 		'not_exist_in_collection' => [], // Аналогично exist_in_collection, но возвращает true, если результат 0
	 * 	),
	 * )
	 */
	public function setRules($data) {
		$this->resetRules();

		$rule_default = array(
			'min_length' => 1,
			'max_length' => 150,
			'greater_than' => null,
			'less_than' => null,
			'greater_than_equal_to' => null,
			'less_than_equal_to' => null,
			'type' => 'string',
			'error' => 'error_data',
			'required' => true,
			'exact_length' => null,
			'date' => null,
			'date_null' => null,
			'time' => null,
			'time_null' => null,
			'json' => null,
			'mobile' => null,
			'iin' => null,
			'in_list' => null,
			'email' => null,
			'callback' => null,
			'alpha_numeric' => null,
			'alpha' => null,
			'is_natural' => null,
			'exist_in_collection' => null,
			'not_exist_in_collection' => null,
		);

		foreach ($data as $key => $d) {
			$d['field'] = $key;

			$rule = array_merge($rule_default, $d);

			if ($rule['required'] === false) {
				$rule['min_length'] = null;
			}
			if ($rule['type'] === 'array') {
				$rule['min_length'] = null;
				$rule['max_length'] = null;
			}
			if ($rule['json'] !== null) {
				$rule['min_length'] = null;
				$rule['max_length'] = 64000;
			}

			$this->_rules[] = $rule;
		}
	}

	/**
	 * Установка правил
	 * @param array $form
	 * @return mixed (string | bool)
	 */
	public function runValidate($form = array()) {
		foreach ($this->_rules as $r) {
			$val = (isset($form[$r['field']])) ? $form[$r['field']] : null;

			if ($r['required'] === true && $val === null) {
				return $r['error'];
			}
			if ($r['required'] === true && !$this->CI->form_validation->required($val)) {
				return $r['error'];
			}

			// если поле необязательное и оно пустое или его нет
			if ($r['required'] === false && (is_null($val) || $val === '')) {
				continue;
			}

			// Проверка типов
			if ($r['type'] === 'int') {
				if (!$this->CI->form_validation->integer($val)) {
					return $r['error'];
				}
			}
			if ($r['type'] === 'float') {
				if (!$this->CI->form_validation->numeric($val)) {
					return $r['error'];
				}
			}
			if ($r['type'] === 'array' && !is_array($val)) {
				return $r['error'];
			}

			$form_validation_list = ['min_length', 'max_length', 'greater_than', 'greater_than_equal_to', 'less_than', 'less_than_equal_to', 'in_list', 'exact_length'];
			foreach ($form_validation_list as $fv_type) {
				if ($r[$fv_type] !== null) {
					// Если через : указано поле, с которым надо сравнивать
					if (strpos($r[$fv_type], ':') === 0) {
						$compare_field = explode(':', $r[$fv_type]);
						$compare_field = (isset($compare_field[1])) ? $compare_field[1] : null;
						if ($compare_field === null) {
							return $r['error'];
						}
						if (!isset($form[$compare_field])) {
							return $r['error'];
						}
						$r[$fv_type] = $form[$compare_field];
					}

					if (in_array($r['type'], ['date', 'time']) && in_array($fv_type, ['greater_than', 'greater_than_equal_to', 'less_than', 'less_than_equal_to'])) {
						if ($r[$fv_type] === 'greater_than' &&
							$val < $r[$fv_type]
						) {
							return $r['error'];
						}
						if ($r[$fv_type] === 'greater_than_equal_to' &&
							$val <= $r[$fv_type]
						) {
							return $r['error'];
						}
						if ($r[$fv_type] === 'less_than' &&
							$val > $r[$fv_type]
						) {
							return $r['error'];
						}
						if ($r[$fv_type] === 'less_than_equal_to' &&
							$val >= $r[$fv_type]
						) {
							return $r['error'];
						}
					} else {
						if (!$this->CI->form_validation->{$fv_type}($val, $r[$fv_type])) {
							return $r['error'];
						}
					}
				}
			}

			if ($r['email'] !== null && !$this->CI->form_validation->valid_email($val)) {
				return $r['error'];
			}
			if ($r['date'] !== null && !$this->date($val, $r['date_null'])) {
				return $r['error'];
			}
			if ($r['time'] !== null && !$this->time($val, $r['time_null'])) {
				return $r['error'];
			}
			if ($r['mobile'] !== null && !$this->mobile($val)) {
				return $r['error'];
			}
			if ($r['iin'] !== null && !$this->iin($val)) {
				return $r['error'];
			}
			if ($r['json'] !== null && !$this->json($val)) {
				return $r['error'];
			}
			if ($r['alpha_numeric'] !== null && !$this->CI->form_validation->alpha_numeric($val)) {
				return $r['error'];
			}
			if ($r['alpha'] !== null && !$this->CI->form_validation->alpha($val)) {
				return $r['error'];
			}
			if ($r['is_natural'] !== null && !$this->CI->form_validation->is_natural($val)) {
				return $r['error'];
			}
			if ($r['callback'] !== null && is_callable($r['callback']) && $r['callback']($val) !== true) {
				return $r['error'];
			}
			if ($r['exist_in_collection'] !== null) {
				$error = $this->existInCollection($r, $form, $r['exist_in_collection']);
				if ($error !== true) {
					return $error;
				}
			}
			if ($r['not_exist_in_collection'] !== null) {
				$error = $this->existInCollection($r, $form, $r['not_exist_in_collection'], 'not_exist_in_collection');
				if ($error !== true) {
					return $error;
				}
			}
		}

		return true;
	}

	/**
	 *
	 * @param array $field Поле с данными
	 * @param array $form Поля формы
	 * @param array $rule_collection Правило для поиска в коллекции
	 * @param string $type_exist Тип проверки в коллекции
	 * @return mixed (string|bool)
	 */
	public function existInCollection($field, $form = [], $rule_collection, $type_exist = 'exist_in_collection') {
		$val = (isset($form[$field['field']])) ? $form[$field['field']] : null;

		$coll_params = [];

		// Основное поле для поиска
		$coll_field = (string) ((!empty($rule_collection['field'])) ? $rule_collection['field'] : $field['field']);
		$coll_params[$coll_field] = $val;

		// Доп поля для поиска
		if ((!empty($rule_collection['params']))) {
			foreach ($rule_collection['params'] as $key => $coll_f) {
				// Если через : указано поле, с которым надо сравнивать
				if (strpos($coll_f, ':') === 0) {
					$compare_field = str_replace(':', '', $coll_f);
					if (!isset($form[$compare_field]) || $compare_field === '') {
						return $field['error'];
					}
					$coll_params[$compare_field] = (string) $form[$compare_field];
				} else {
					$coll_params[$key] = (string) $coll_f;
				}
			}
		}

		if (empty($rule_collection['cache'])) {
			$method = 'getTotal';
		} else {
			$method = 'get';
		}
		if (!empty($rule_collection['method'])) {
			$method = $rule_collection['method'];
		}

		$cache_temp_key = $rule_collection['class'] . '::' . $method . '_' . http_build_query($coll_params);

		// Ищем в кеше (временном и memcache)
		if (!empty($rule_collection['cache'])) {
			$cache_temp_result = $this->CI->cache_tempory->get($cache_temp_key);
		}
		$collection_result = (isset($cache_temp_result) && $cache_temp_result !== false) ? $cache_temp_result : $rule_collection['class']::{$method}($coll_params);

		if (
			($type_exist === 'not_exist_in_collection' && $collection_result) ||
			($type_exist === 'exist_in_collection' && !$collection_result)
		) {
			return $field['error'];
		}

		// Сохраняем во временый кеш
		if (!empty($rule_collection['cache'])) {
			$cache_temp_data = $collection_result;

			$this->CI->cache_tempory->save($cache_temp_key, $cache_temp_data);
		}

		return true;
	}

	/**
	 * Сброс правил для валидации
	 */
	public function resetRules() {
		$this->_rules = array();
	}

}
