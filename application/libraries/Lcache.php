<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class LCache {

	protected $CI = null;

	public function __construct($config = array()) {
		$this->CI = & get_instance();
	}

	public function get($id) {
		$data = $this->CI->cache_tempory->get($id);

		if ($data !== false) {
			return $data;
		}

		$data = $this->CI->cache->get($id);
		$this->CI->cache_tempory->save($id, $data);

		return $data;
	}

	/**
	 * Сохранение данных
	 * @param string $id Ключ
	 * @param mixed (array|string) Данные
	 * @param int $ttl Время хранения в секундах
	 * @param bool $tmp Хранение во временной памяти
	 * @return bool
	 */
	public function save($id, $data, $ttl = 60, $tmp = true) {
		if ($tmp) {
			$this->CI->cache_tempory->save($id, $data);
		}

		$result = $this->CI->cache->save($id, $data, $ttl);

		$cache_type = $this->CI->config->item('cache_type');

		// Меняем права файлов на www-data
		if ((!$cache_type || $cache_type === 'file') && is_cli() && PHP_OS !== 'WINNT') {
			$path = $this->CI->config->item('cache_path');
			$cache_path = ($path === '') ? APPPATH . 'cache/' : $path;
			chown($cache_path . $id, 'www-data');
		}

		return $result;
	}

	/**
	 * Удаление данных по ключу
	 * @param string $id
	 * @return bool
	 */
	public function delete($id) {
		$this->CI->cache_tempory->delete($id);
		return $this->CI->cache->delete($id);
	}

	/**
	 * Очистка всего кэша
	 * @return bool
	 */
	public function clean() {
		$this->CI->cache_tempory->clean();
		return $this->CI->cache->clean();
	}

	public function cache_info($type = 'user') {
		return $this->CI->cache->cache_info($type);
	}

	public function get_metadata($id) {
		return $this->CI->cache->get_metadata($id);
	}

	public function is_supported($driver) {
		return $this->CI->cache->is_supported($driver);
	}

	/**
	 * Создание ключа на основе данных
	 * @param array
	 * @return string
	 */
	public function createId($param = array()) {
		$array_params = http_build_query($param);

		return hash("crc32b", $array_params);
	}

}
