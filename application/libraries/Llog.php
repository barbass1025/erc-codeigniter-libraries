<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Класс логов
 * Основан на system/library/Log.php
 * Хранит данные в json-формате
 */
class LLog extends CI_Log {

	protected $_types = array();

	public function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * Write Log File
	 *
	 * Generally this function will be called using the global log_message() function
	 *
	 * @param	string	the error level
	 * @param	string | array	the error message
	 * @return	bool
	 */
	public function write_log($type = 'standart', $msg) {
		if ($this->_enabled === FALSE) {
			return FALSE;
		}

		$type = strtolower($type);

		if (in_array($type, $this->_types)) {
			$file = 'log-' . $type . '-' . date('Y-m-d') . '.' . $this->_file_ext;
			$filepath = $this->_log_path . $type . '/' . $file;
		} else {
			$file = 'log-' . date('Y-m-d') . '.' . $this->_file_ext;
			$filepath = $this->_log_path . '/' . $file;
		}

		// Заносим логи по типу в отдельные папки
		if (!is_dir($this->_log_path . $type) && in_array($type, $this->_types)) {
			mkdir($this->_log_path . $type, 0777);
		}

		$message = '';

		if (!file_exists($filepath)) {
			$newfile = TRUE;
			if ($this->_file_ext === 'php') {
				$message .= "<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>\n\n";
			}
		}

		if (!$fp = @fopen($filepath, 'ab')) {
			return FALSE;
		}


		$data = array(
			'date' => date($this->_date_fmt),
			'msg' => $msg,
		);

		$message .= json_encode($data, JSON_UNESCAPED_UNICODE) . "\n";

		flock($fp, LOCK_EX);

		for ($written = 0, $length = strlen($message); $written < $length; $written += $result) {
			if (($result = fwrite($fp, substr($message, $written))) === FALSE) {
				break;
			}
		}

		flock($fp, LOCK_UN);
		fclose($fp);

		if (isset($newfile) && $newfile === TRUE) {
			chmod($filepath, $this->_file_permissions);
		}

		return is_int($result);
	}

	/**
	 * Чтение логов
	 * @param string $file
	 * @param string $type
	 * @param string $template
	 * @return array
	 */
	public function read_log($file = NULL, $type = 'default', $template = null) {
		if ($file === null && $template === null) {
			if ($type !== '' && $type !== 'all') {
				$filepath = 'log-' . $type . '-*.' . $this->_file_ext;
				// если пустой, то читаем логи фреймворка
			} elseif ($type === '') {
				// 2 нужно, чтобы ограничить поиск (стандартные логи идут как log-2013-02-01.php)
				$filepath = 'log-2*.' . $this->_file_ext;
				// иначе читаем все логи
			} elseif ($type === 'all') {
				$filepath = 'log-*.' . $this->_file_ext;
			}
		} elseif ($file !== null) {
			$filepath = $file;
		} elseif ($template !== null) {
			$filepath = 'log-' . $template . '.' . $this->_file_ext;
		}

		$path_types = $this->_types;
		array_push($path_types, '');
		$files = array();
		foreach ($path_types as $p) {
			if (empty($p)) {
				$path = $this->_log_path . $filepath;
			} else {
				$path = $this->_log_path . $p . '/' . $filepath;
			}

			$files_p = glob($path);

			if ($files_p) {
				$files = array_merge($files, $files_p);
			}
		}

		$data = array();
		if ($files) {
			foreach ($files as $file) {
				$handle = @fopen($file, "r");
				if (!$handle) {
					$data[$file]['error'] = false;
					continue;
				}
				$i = 0;
				while (($f = fgets($handle)) !== false) {
					// Пропускаем первые 2 строки
					if ($i > 1) {
						$msg = json_decode($f, true);
						// если не json, то простое сообщение
						if (!is_array($msg)) {
							$msg = $f;
						}

						$data[$file]['data'][] = $msg;
					}
					$i++;
				}

				fclose($handle);
			}
		}

		return $data;
	}

	/**
	 * Возвращает допустимые типы логов
	 * @return array
	 */
	public function getTypes() {
		return (!empty($this->_types)) ? $this->_types : array();
	}

	/**
	 * Возвращает каталог для хранения логов
	 * @return string
	 */
	public function getPath() {
		return $this->_log_path;
	}

	/**
	 * Возвращает расширение лог-файлов
	 * @return string
	 */
	public function getLogExt() {
		return $this->_file_ext;
	}

}
